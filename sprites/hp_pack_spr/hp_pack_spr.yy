{
    "id": "d24f1c30-031d-4fec-a6b1-8b95ff508245",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hp_pack_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c9097ee-a4ab-4047-902e-26c4445418fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d24f1c30-031d-4fec-a6b1-8b95ff508245",
            "compositeImage": {
                "id": "a2001faf-4e7b-495d-8626-966e82e0b4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9097ee-a4ab-4047-902e-26c4445418fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c16e0c0-6dda-4fdb-853e-472aad8c7e53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9097ee-a4ab-4047-902e-26c4445418fd",
                    "LayerId": "4c8712dc-ca3c-464e-a6f7-843eb3759618"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c8712dc-ca3c-464e-a6f7-843eb3759618",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d24f1c30-031d-4fec-a6b1-8b95ff508245",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}