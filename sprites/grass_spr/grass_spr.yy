{
    "id": "2ab7267d-d23c-4725-ae3d-81cbee4516c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0efea329-3185-4c11-aaec-307505d531bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ab7267d-d23c-4725-ae3d-81cbee4516c6",
            "compositeImage": {
                "id": "59fa351c-7e33-4dbb-817a-b1bc85a72995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0efea329-3185-4c11-aaec-307505d531bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2341fa0f-40d4-4a64-be8b-ea91945f78b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efea329-3185-4c11-aaec-307505d531bd",
                    "LayerId": "ba8f4a1c-aba6-4394-9c5c-191e6ce372d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ba8f4a1c-aba6-4394-9c5c-191e6ce372d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ab7267d-d23c-4725-ae3d-81cbee4516c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}