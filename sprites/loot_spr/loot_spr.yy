{
    "id": "74ce1aad-4ff1-4710-a479-c3f3040ce1c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "loot_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf200d46-6a1c-44dc-9054-10104c918583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74ce1aad-4ff1-4710-a479-c3f3040ce1c2",
            "compositeImage": {
                "id": "1e0248b3-9c49-4a38-a7b8-f82436222a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf200d46-6a1c-44dc-9054-10104c918583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d6c7476-df25-4502-b658-8cd12b7c86b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf200d46-6a1c-44dc-9054-10104c918583",
                    "LayerId": "2ef97ea7-9090-40af-8bd8-ed75604acc85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2ef97ea7-9090-40af-8bd8-ed75604acc85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74ce1aad-4ff1-4710-a479-c3f3040ce1c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}