{
    "id": "5c2d0620-91b6-4ea0-bfc0-7db83f9088b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hp_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b99a31a6-565f-4332-9c40-64f818932e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c2d0620-91b6-4ea0-bfc0-7db83f9088b1",
            "compositeImage": {
                "id": "f4885463-f9c6-4189-8521-f574eaddce8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b99a31a6-565f-4332-9c40-64f818932e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e40f2a5-bea5-422b-ac0a-7ebdb9eed9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b99a31a6-565f-4332-9c40-64f818932e88",
                    "LayerId": "fc69da15-551d-49e2-bcb4-225862fc1105"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fc69da15-551d-49e2-bcb4-225862fc1105",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c2d0620-91b6-4ea0-bfc0-7db83f9088b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}