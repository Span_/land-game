{
    "id": "33821dcd-52c2-432f-b01b-955ba43bbceb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "neophyte_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 14,
    "bbox_right": 46,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "114d0e9e-458a-4dae-a35e-7b922c8a5da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33821dcd-52c2-432f-b01b-955ba43bbceb",
            "compositeImage": {
                "id": "459426e1-5437-459c-9791-45a3de6c808d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114d0e9e-458a-4dae-a35e-7b922c8a5da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf793f47-393c-4f0e-aaab-f40317fe23d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114d0e9e-458a-4dae-a35e-7b922c8a5da4",
                    "LayerId": "aac53b46-a120-44f9-971b-a2579b80ef57"
                }
            ]
        },
        {
            "id": "f0c6a0e5-86b6-4c52-9cc2-cf54709b15bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33821dcd-52c2-432f-b01b-955ba43bbceb",
            "compositeImage": {
                "id": "1fe61c50-53f7-4a41-9eed-fc6826f02982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c6a0e5-86b6-4c52-9cc2-cf54709b15bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f87c317-f7cb-4313-9121-3d27d1d8d4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c6a0e5-86b6-4c52-9cc2-cf54709b15bd",
                    "LayerId": "aac53b46-a120-44f9-971b-a2579b80ef57"
                }
            ]
        },
        {
            "id": "f34c1aae-3fba-4437-b1d6-c034ba6bf3af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33821dcd-52c2-432f-b01b-955ba43bbceb",
            "compositeImage": {
                "id": "870c9a2e-efcd-4670-ba08-d911e2736b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34c1aae-3fba-4437-b1d6-c034ba6bf3af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f21ac593-9876-46c3-b468-6725133b8c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34c1aae-3fba-4437-b1d6-c034ba6bf3af",
                    "LayerId": "aac53b46-a120-44f9-971b-a2579b80ef57"
                }
            ]
        },
        {
            "id": "bed56226-ac7c-431a-8263-6e75e51d4a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33821dcd-52c2-432f-b01b-955ba43bbceb",
            "compositeImage": {
                "id": "a8631215-36e1-482b-8933-613875939cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bed56226-ac7c-431a-8263-6e75e51d4a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff894bfd-376d-4da7-bfc6-f719d770e010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bed56226-ac7c-431a-8263-6e75e51d4a23",
                    "LayerId": "aac53b46-a120-44f9-971b-a2579b80ef57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aac53b46-a120-44f9-971b-a2579b80ef57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33821dcd-52c2-432f-b01b-955ba43bbceb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}