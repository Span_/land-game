{
    "id": "adcd0cbc-ca35-4acf-839b-db86bc9c98eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_sky_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a528f93-c6ad-4873-a17c-a82f4d83eb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adcd0cbc-ca35-4acf-839b-db86bc9c98eb",
            "compositeImage": {
                "id": "f2a149d2-eaef-4435-8dc2-bc8faafa6831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a528f93-c6ad-4873-a17c-a82f4d83eb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b841d630-9be1-4c15-9900-4339ff624269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a528f93-c6ad-4873-a17c-a82f4d83eb5b",
                    "LayerId": "38cd33d8-42f2-4ed8-8d43-e80fea555ad8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "38cd33d8-42f2-4ed8-8d43-e80fea555ad8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adcd0cbc-ca35-4acf-839b-db86bc9c98eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}