{
    "id": "525533d3-4736-4f33-a89c-8dcd3f1c55bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "exp_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d572773-0cce-4251-8426-6ccf9e3fbea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "525533d3-4736-4f33-a89c-8dcd3f1c55bd",
            "compositeImage": {
                "id": "e66b611a-521c-4fdc-8b1c-befc99d1ac62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d572773-0cce-4251-8426-6ccf9e3fbea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4cf1a07-5ab4-4856-8741-ea7bb0fd4cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d572773-0cce-4251-8426-6ccf9e3fbea8",
                    "LayerId": "38051fdb-2408-4e21-a15f-582e9ac20d2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "38051fdb-2408-4e21-a15f-582e9ac20d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "525533d3-4736-4f33-a89c-8dcd3f1c55bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}