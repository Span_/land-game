{
    "id": "553f5773-42a3-4ae2-9e81-ebed83882ef5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "big_boi_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90fb9aad-a57c-43e9-a4f0-2b0cf58ae94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "553f5773-42a3-4ae2-9e81-ebed83882ef5",
            "compositeImage": {
                "id": "7c1035fd-4a36-4e39-b619-5a0ef972fb48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90fb9aad-a57c-43e9-a4f0-2b0cf58ae94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d7b697-e2ae-487c-a380-3ce3cc33c05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90fb9aad-a57c-43e9-a4f0-2b0cf58ae94d",
                    "LayerId": "b1a0ec90-a774-43e4-805d-81b1eaa23199"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "b1a0ec90-a774-43e4-805d-81b1eaa23199",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "553f5773-42a3-4ae2-9e81-ebed83882ef5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}