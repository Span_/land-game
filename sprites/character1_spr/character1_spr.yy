{
    "id": "604c3885-a1f5-4127-adbc-2af797813544",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "character1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 20,
    "bbox_right": 37,
    "bbox_top": 25,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "204c1535-52b8-4669-95a8-b6d092c68cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "604c3885-a1f5-4127-adbc-2af797813544",
            "compositeImage": {
                "id": "571d3595-a769-46c6-9f5b-c38d33ea8e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "204c1535-52b8-4669-95a8-b6d092c68cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3c9c9e-90b7-4dd9-8756-b79dd840e965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "204c1535-52b8-4669-95a8-b6d092c68cff",
                    "LayerId": "a810457b-80f3-4d9f-869d-a0fc1f6552e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a810457b-80f3-4d9f-869d-a0fc1f6552e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "604c3885-a1f5-4127-adbc-2af797813544",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}