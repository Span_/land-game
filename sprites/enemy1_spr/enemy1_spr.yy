{
    "id": "69836573-ca16-4ea9-a24a-cceef5dd426f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 21,
    "bbox_right": 39,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b06dbc85-0ba2-416d-959e-b77d2246fe70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69836573-ca16-4ea9-a24a-cceef5dd426f",
            "compositeImage": {
                "id": "d5ef6d49-f866-4fe7-a3e6-3af87a9072ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06dbc85-0ba2-416d-959e-b77d2246fe70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cebc7630-5c1a-4623-b050-bec99c255a30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06dbc85-0ba2-416d-959e-b77d2246fe70",
                    "LayerId": "7db29300-05a3-46ef-b5e9-130fb48b66fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7db29300-05a3-46ef-b5e9-130fb48b66fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69836573-ca16-4ea9-a24a-cceef5dd426f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}