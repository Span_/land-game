{
    "id": "0fe69be9-e7f0-4f07-b6e9-015297e19f6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ext_mag_1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6bc6f96-f9d8-4b13-8b9b-0d03dfaa7b4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fe69be9-e7f0-4f07-b6e9-015297e19f6f",
            "compositeImage": {
                "id": "d258482e-3190-4352-8729-ef4058115c71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6bc6f96-f9d8-4b13-8b9b-0d03dfaa7b4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de90dce-3a64-42e6-9289-f9b2b03848e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bc6f96-f9d8-4b13-8b9b-0d03dfaa7b4b",
                    "LayerId": "28c70698-490e-4d97-a324-1885c4621c22"
                },
                {
                    "id": "a88d6d56-a778-4d7c-a79f-b2421f437043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bc6f96-f9d8-4b13-8b9b-0d03dfaa7b4b",
                    "LayerId": "415e2786-93f9-4f31-9c22-e52696f95364"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "28c70698-490e-4d97-a324-1885c4621c22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fe69be9-e7f0-4f07-b6e9-015297e19f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "415e2786-93f9-4f31-9c22-e52696f95364",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fe69be9-e7f0-4f07-b6e9-015297e19f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}