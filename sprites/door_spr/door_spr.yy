{
    "id": "84cafbd7-4d72-4f35-8d32-65f9882ffb26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "door_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "542b63fb-9fb2-4e79-b41f-80220fa64bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84cafbd7-4d72-4f35-8d32-65f9882ffb26",
            "compositeImage": {
                "id": "d1e7b444-b16c-40b7-bb52-86deba333194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542b63fb-9fb2-4e79-b41f-80220fa64bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dfa4424-a8f6-4f88-b260-6de21217cdc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542b63fb-9fb2-4e79-b41f-80220fa64bf3",
                    "LayerId": "6cef6cf0-1502-4810-bf31-e3f0af65983f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6cef6cf0-1502-4810-bf31-e3f0af65983f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84cafbd7-4d72-4f35-8d32-65f9882ffb26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}