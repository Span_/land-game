{
    "id": "66fbf24c-65bf-4d8b-8aaf-c0277e509a26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wall_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8de3adb-515d-426d-897e-63a9a72ce651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66fbf24c-65bf-4d8b-8aaf-c0277e509a26",
            "compositeImage": {
                "id": "7a175cbd-838b-409c-930f-c911a12261f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8de3adb-515d-426d-897e-63a9a72ce651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc14398-275f-4f15-96bc-e02c37a75ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8de3adb-515d-426d-897e-63a9a72ce651",
                    "LayerId": "bd22fa81-db73-4f19-b1eb-56073f8d5903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bd22fa81-db73-4f19-b1eb-56073f8d5903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66fbf24c-65bf-4d8b-8aaf-c0277e509a26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}