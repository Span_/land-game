///@description make loot obj with random direction, upward velocity

///@param obj - object to spawn
///@param x - object to spawn's x position
///@param y - object to spawn's y position 

var o = argument0;
var x_o = argument1;
var y_o = argument2;

var in = instance_create_layer(x_o,y_o,global.loot_layer,o);

with (in) {
	
	//pick random angle 0 - pi
	var ra = random_range(0.0,pi);
	v_x = v_j * cos(ra);
	v_y = -1.0 * v_j * sin(ra);
	
}