///@description handle player collision / valid movements

///@param delta_time - time since last step

var dt = argument0;


//proposed x,y for next step. collision check.
var x_step = x + v_x * dt * global.mtopx;
var y_step = y + v_y * dt * global.mtopx;

//x,y for sprite's step
var x1s_step = x_step + sprite_get_bbox_left(sprite_index);
var y1s_step = y_step + sprite_get_bbox_top(sprite_index);
var x2s_step = x_step + sprite_get_bbox_right(sprite_index);
var y2s_step = y_step + sprite_get_bbox_bottom(sprite_index);

//handle colisions

if (noone == collision_line(x + sprite_get_bbox_left(sprite_index),
					y + 1 + sprite_get_bbox_bottom(sprite_index),
					x + sprite_get_bbox_right(sprite_index),
					y + 1 + sprite_get_bbox_bottom(sprite_index),collidable_obj,true,true)){
	//fall if walk off a ledge
	ff = true;
	
	
} 
//else {
//	ff = false;
//	v_y = 0;
//}

if ( v_y > 0.0) { //if going down:
	var cl = ds_list_create();
	//just looking at the y-direction movement here
	collision_line_list(x + sprite_get_bbox_left(sprite_index),y2s_step,
					x + sprite_get_bbox_right(sprite_index),y2s_step,
					collidable_obj,true,true,cl,false);

	for (var i = 0; i < ds_list_size(cl); i++){
		//it is colliding with a thing -> see if we can move a shorter distance without colliding.
		var ci = cl[|i];
	
		var ci_x_pos, ci_y_pos;
		var ci_bbox_right, ci_bbox_left, ci_bbox_bottom, ci_bbox_top;
	
		with (ci) {
			//get attributes of ci - the collided object
			ci_x_pos = x;
			ci_y_pos = y;
		
			ci_bbox_right = sprite_get_bbox_right(sprite_index);
			ci_bbox_left = sprite_get_bbox_left(sprite_index);
			ci_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
			ci_bbox_top = sprite_get_bbox_top(sprite_index);
		
		}
		if (noone == collision_line(x + sprite_get_bbox_left(sprite_index),
						ci_y_pos + ci_bbox_top - 1,
						x + sprite_get_bbox_right(sprite_index),
						ci_y_pos + ci_bbox_top - 1,collidable_obj,true,true)){
			//move remainder
			y = ci_y_pos + ci_bbox_top - sprite_get_bbox_bottom(sprite_index) - 1;
			ff = false;
			v_y = 0.0;
		}
	}
	if (ds_list_size(cl) == 0){
		//move in the y-direction
		y = y_step;
	}
	ds_list_destroy(cl);
}
//handle upward movement
if (v_y < 0.0) {
	//apply upward movement
	if(noone == collision_line(x + sprite_get_bbox_left(sprite_index),y1s_step,
								x + sprite_get_bbox_right(sprite_index), y1s_step,
								collidable_obj,true,true)){
		y = y_step;
	} else {
		v_y = 0.0; //hit your head
		//try to move remainder
	}
}



//if(position_meeting(x,y-1,collidable_obj)) y--;

//handle left & right movement in the case that the proposed step is longer than the distance to...
//...the wall

var cl = ds_list_create();
//just looking at the x-direction (left) movement here
if ( v_x < 0.0) {
	
	collision_line_list(x1s_step,y + sprite_get_bbox_top(sprite_index),
				x1s_step,y + sprite_get_bbox_bottom(sprite_index),
				collidable_obj,true,true,cl,false);
				
	for (var i = 0; i < ds_list_size(cl); i++){
		//it is colliding with a thing -> see if we can move a shorter distance without colliding.
		var ci = cl[|i];
	
		var ci_x_pos, ci_y_pos;
		var ci_bbox_right, ci_bbox_left, ci_bbox_bottom, ci_bbox_top;
	
		with (ci) {
			//get attributes of ci - the collided object
			ci_x_pos = x;
			ci_y_pos = y;
		
			ci_bbox_right = sprite_get_bbox_right(sprite_index);
			ci_bbox_left = sprite_get_bbox_left(sprite_index);
			ci_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
			ci_bbox_top = sprite_get_bbox_top(sprite_index);
		
		}
		if (noone == collision_line(ci_x_pos + ci_bbox_right + 1,
						y + sprite_get_bbox_top(sprite_index),
						ci_x_pos + ci_bbox_right + 1,
						y + sprite_get_bbox_bottom(sprite_index),collidable_obj,true,true)){
			//move remainder
			x = ci_x_pos + ci_bbox_right - sprite_get_bbox_left(sprite_index) + 1;
			v_x = 0.0;
		}
	}
	if (ds_list_size(cl) == 0){
		//move in the x-direction
		x = x_step;
	}
}
ds_list_destroy(cl);



var cl = ds_list_create();
//just looking at the x-direction (RIGHT) movement here
if ( v_x > 0.0) {
	
	collision_line_list(ceil( x2s_step),y + sprite_get_bbox_top(sprite_index),
				ceil(x2s_step),y + sprite_get_bbox_bottom(sprite_index),
				collidable_obj,true,true,cl,false);
				
	for (var i = 0; i < ds_list_size(cl); i++){
		//it is colliding with a thing -> see if we can move a shorter distance without colliding.
		var ci = cl[|i];
	
		var ci_x_pos, ci_y_pos;
		var ci_bbox_right, ci_bbox_left, ci_bbox_bottom, ci_bbox_top;
	
		with (ci) {
			//get attributes of ci - the collided object
			ci_x_pos = x;
			ci_y_pos = y;
		
			ci_bbox_right = sprite_get_bbox_right(sprite_index);
			ci_bbox_left = sprite_get_bbox_left(sprite_index);
			ci_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
			ci_bbox_top = sprite_get_bbox_top(sprite_index);
		
		}
		if (noone == collision_line(ci_x_pos + ci_bbox_left - 1,
						y + sprite_get_bbox_top(sprite_index),
						ci_x_pos + ci_bbox_left - 1,
						y + sprite_get_bbox_bottom(sprite_index),collidable_obj,true,true)){
			//move remainder
			x = ci_x_pos + ci_bbox_left - sprite_get_bbox_right(sprite_index) - 1;
			v_x = 0.0;
		}
	}
	if (ds_list_size(cl) == 0){
		//move in the x-direction
		x = x_step;
	}
}
ds_list_destroy(cl);