

//physics_world_gravity(0, 20);

global.mtopx = 32; //32 pixels per meter
global.msec = 1000000.0;//a millionth of a second

global.hk_left = ord("A");
global.hk_right = ord("D");
global.hk_jump = vk_space;
global.hk_crouch = ord("S");

global.hk_activate = ord("F");

global.hk_shoot_left = ord("J");
global.hk_shoot_right = ord("L");
global.hk_melee_left = ord("U");
global.hk_melee_right = ord("O");

global.hk_reload = ord("R");

global.hk_item_1 = ord("1");
global.hk_item_2 = ord("2");
global.hk_item_3 = ord("3");
global.hk_item_4 = ord("4");
global.hk_item_5 = ord("5");
global.hk_item_6 = ord("6");
global.hk_item_7 = ord("7");
global.hk_item_8 = ord("8");
global.hk_item_9 = ord("9");
global.hk_item_0 = ord("0");

global.ui_layer = "ui_layer";
global.loot_layer = "loot_layer";
global.enemy_layer = "enemy_layer";

global.hp_pack_value = 20;
global.hp_pack_rate = 0.5; //probability to drop
global.exp_val_enemy = 10;
global.ext_mag_1_value = 5;

global.spawn_loc = ds_list_create();