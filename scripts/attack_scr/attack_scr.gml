
///@param x1
///@param y1
///@param x2
///@param y2
///@param cooldown
///@param damage

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var cd = argument4;
var dmg = argument5;

cooldown = cd;
var dist;
var cl = ds_list_create();
collision_line_list(x1,y1,x2,y2,collidable_obj,true,true,cl,true);
if (ds_list_size(cl) != 0) {
	//get the nearest distance one and set as max distance for shot
	with (cl[|0]) {
		if (x1 < x2) dist = x + sprite_get_bbox_left(sprite_index);
		else dist = x + sprite_get_bbox_right(sprite_index);
	}
} else dist = x2; //max dist if nothing blocking
ds_list_destroy(cl);
//now check for nearest enemy
cl = ds_list_create();
	
collision_line_list(x1,y1,dist,y2,enemy_obj,true,true,cl,true);
if (ds_list_size(cl) != 0) {
	//if there is an enemy, apply damage 
	var hp_to_lose = dmg;
	with (cl[|0]) {
		hp -= hp_to_lose;
			
		//todo: add particle effect
	}
}
ds_list_destroy(cl);