///@description speed up & slow down

///@param v - current velocity
///@param v_max - target velocity
///@param a - should be positive
///@param dt - time since last tick

var v = argument0;
var v_max = argument1;
var a = argument2;
var dt = argument3;

if( v < v_max) {
	//do last bit of accelleration, careful not to go above max speed
	if ( v + a * dt > v_max) v  = v_max;
	else v += a * dt;
}else if ( v > v_max) {
	//negative acceleration
	if ( v - a * dt < v_max) v  = v_max;
	else v -= a * dt;
}

//return new velocity
return v;