/// @description player bonus

var p = instance_nearest(x,y,player_obj);
if (p != noone) {
	with (p) {
		experience += global.exp_val_enemy;
	}
}