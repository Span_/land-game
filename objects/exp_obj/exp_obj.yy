{
    "id": "60c2d513-4449-4087-acb9-584e31f67a00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "exp_obj",
    "eventList": [
        {
            "id": "41899e2c-476f-4445-a77d-e0def9a12bd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "60c2d513-4449-4087-acb9-584e31f67a00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "525533d3-4736-4f33-a89c-8dcd3f1c55bd",
    "visible": true
}