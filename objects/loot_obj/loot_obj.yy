{
    "id": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "loot_obj",
    "eventList": [
        {
            "id": "12393a8c-2d9b-4fce-9d1e-008c3f3f55c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d"
        },
        {
            "id": "4c5d7e09-16dd-4c78-b3d2-712707d73875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "74ce1aad-4ff1-4710-a479-c3f3040ce1c2",
    "visible": true
}