/// @description drops from enemies, picked up by player


//m/s
v_x = 0.0;
v_y = 0.0;

//m/s^2
a_x = 0.0;
a_y = 0.0;

//m/s max speed
v_x_max = 6.0; //max speed while running
v_y_max = 6.0;

//m/s^2
a_x_max = 10.0; //acceleration by running
a_x_ff = 10.0; //accelation while in free fall
a_y_max = 10.0; //acceleration by running
a_y_ff = 10.0; //accelation while in free fall

//gravity acceleration
a_g = 10.0;
//jump initial speed
v_j = 6.0;

ff = false; //free fall

fly_range = 100; //px when flies to player
