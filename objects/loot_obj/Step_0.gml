/// @description loot obj - handle falling & pickup

outofbounds_scr();

var dt = delta_time / global.msec; //time since last tick in seconds

var p = instance_nearest(x,y,player_obj);
if (p != noone) {
	with (p) {
		var p_x_pos = x;
		var p_y_pos = y;
		
		//var p_bbox_right = sprite_get_bbox_right(sprite_index);
		var p_bbox_left = sprite_get_bbox_left(sprite_index);
		//var p_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
		var p_bbox_top = sprite_get_bbox_top(sprite_index);
	}
	var x2 = p_x_pos + p_bbox_left;
	var y2 = p_y_pos + p_bbox_top;
	var x1 = x + sprite_get_bbox_left(sprite_index);
	var y1 = y + sprite_get_bbox_top(sprite_index);
	
	var dist = sqrt(sqr(x1 - x2) + sqr(y1 - y2));
	var x_normalizer = (x2 - x1) / dist; //x component of unit vector
	var y_normalizer = (y2 - y1) / dist; //y component of unit vector
	
	if (dist <= fly_range){
		ff = false;
		var v_x_max_cur = v_x_max * x_normalizer; //scale the velocity vector
		var v_y_max_cur = v_y_max * y_normalizer;
		
		//player is within range, so fly towards it
		v_x = accelerate_scr(v_x, v_x_max_cur, a_x_max, dt);
		v_y = accelerate_scr(v_y, v_y_max_cur, a_y_max, dt);
		
	} else if (!ff) {
		v_x = accelerate_scr(v_x, 0.0, a_x_max, dt);//slow to a stop
		//ff = true;
		//player no longer in range, fall back down.
	}
} else if (!ff) {
	v_x = accelerate_scr(v_x, 0.0, a_x_max, dt);//slow to a stop
}
if  (ff) { //fall due to gravity
	v_y += a_g * dt;
} 

collision_scr(dt);
var mid_x = x + (sprite_get_bbox_left(sprite_index) + sprite_get_bbox_right(sprite_index)) / 2.0;
var mid_y = y + (sprite_get_bbox_top(sprite_index) + sprite_get_bbox_bottom(sprite_index)) / 2.0;
if (collision_point(mid_x,mid_y,player_obj, true, true)) {
	instance_destroy();
}