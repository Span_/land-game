{
    "id": "c5ccdb46-4ad0-42fd-9c1e-a6f3cb0625e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "hp_pack_obj",
    "eventList": [
        {
            "id": "3d2a316d-dab1-4216-8584-747d3ffc378e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c5ccdb46-4ad0-42fd-9c1e-a6f3cb0625e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d24f1c30-031d-4fec-a6b1-8b95ff508245",
    "visible": true
}