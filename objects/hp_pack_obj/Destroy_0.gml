/// @description player benefit

var p = instance_nearest(x,y,player_obj);
if (p != noone) {
	with (p) {
		if (hp < hp_max) {
			if (hp + global.hp_pack_value > hp_max) { 
				hp = hp_max;
			} else {
				hp += global.hp_pack_value;
			}
			
		}
	}
}