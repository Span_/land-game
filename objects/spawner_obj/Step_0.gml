/// @description spawner step

var dt = delta_time / global.msec; //time since last tick in seconds


if (spawn_time_left <= 0) {
	spawn_time_left = spawn_rate;
	var obj = enemies[|irandom_range(0,ds_list_size(enemies) - 1)];
	
	//var loc = ds_list_create();
	if(ds_list_size(global.spawn_loc) > 0) {
		//get a random spawn location
		ds_list_shuffle(global.spawn_loc);
		var e = instance_create_layer(0,0,global.enemy_layer,obj);
		with (e) { //get variables needed for the enemy collision box
			//var e_x = x;
			//var e_y = y;
			//var e_bbox_left = sprite_get_bbox_left(sprite_index);
			var e_bbox_right = sprite_get_bbox_right(sprite_index);
			var e_bbox_top = sprite_get_bbox_top(sprite_index);
			//var e_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
		}
		for (var i = 0; i < ds_list_size(global.spawn_loc); i ++) {
			//check if it is clear to spawn
			var loc = global.spawn_loc[|i];
			//create enemy
			var xplace = loc.x - obj.sprite_width;
			var yplace = loc.y - obj.sprite_height;
			
			//check that the placement is clear
			if (noone == collision_rectangle(loc.x, loc.y - 1, loc.x + e_bbox_right, loc.y - 1 - e_bbox_top,
												collidable_obj,true,true)) {
				with (e) {
					x = loc.x;
					y = loc.y - 1 - sprite_height;
				}
				break;
			}
		}
		//var loc = global.spawn_loc[|irandom_range(0,ds_list_size(global.spawn_loc))];
		
	}
	
	//instance_create_layer(x_s,y_s,global.enemy_layer,obj);
} else {
	spawn_time_left -= dt;
} 