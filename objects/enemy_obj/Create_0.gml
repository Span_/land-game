/// @description 

//m/s
v_x = 0;
v_y = 0;

//m/s^2
a_x = 0;
a_y = 0;

//m/s max speed
v_x_max = 2; //max speed while running
//m/s^2
a_x_max = 2; //acceleration by running

//gravity acceleration
a_g = 10;
//jump initial speed
v_j = 5;

ff = false; //free fall

mass = 90; //kg

hp = 100;

melee_dmg = 10;
melee_rng = 8;
melee_cooldown = 1.0;
cooldown = 0.0;

aggro_range = 200;