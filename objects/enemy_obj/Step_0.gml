/// @description enemy step

//am i become die?
//hp check
if (hp <= 0) {
	instance_destroy(id);
}

outofbounds_scr();


var dt = delta_time / global.msec; //time since last tick in seconds
var p = instance_nearest(x + sprite_get_bbox_left(sprite_index),
			y + sprite_get_bbox_bottom(sprite_index),player_obj);

if (cooldown > 0.0) {
	cooldown -= dt;
} else {
	cooldown = 0.0;
}

if (p != noone){
	var p_x, p_y;
	var p_bbox_left, p_bbox_right, p_bbox_top, p_bbox_bottom;
	with (p) {
		p_x = x;
		p_y = y;
		p_bbox_left = sprite_get_bbox_left(sprite_index);
		p_bbox_right = sprite_get_bbox_right(sprite_index);
		p_bbox_top = sprite_get_bbox_top(sprite_index);
		p_bbox_bottom = sprite_get_bbox_bottom(sprite_index);
	}
	
	//check for asolute distance first
	if (abs(y-p_y) <= aggro_range){
		//if the player is to our left, go left
		if (p_x + p_bbox_left < x + sprite_get_bbox_left(sprite_index)){
		
			v_x = accelerate_scr(v_x,-1.0 * v_x_max ,a_x_max,dt);
		
		} else if (p_x + p_bbox_left > x + sprite_get_bbox_left(sprite_index)){
			v_x = accelerate_scr(v_x,v_x_max ,a_x_max,dt);
		
		}
	} else {
		//move randomly
		//accelerate_scr((1 - irandom(2)) * v_x, v_x_max, a_x_max, dt);
	}
	
	//attack player if close
	if (cooldown <= 0.0) {
		//confirm player is with up-down range to be attacked
		if ((p_y + p_bbox_top >= y + sprite_get_bbox_bottom(sprite_index) //down
				&& p_y + p_bbox_top <= y + sprite_get_bbox_top(sprite_index))
				|| (p_y + p_bbox_bottom >= y + sprite_get_bbox_bottom(sprite_index) //up
				&& p_y + p_bbox_bottom <= y + sprite_get_bbox_top(sprite_index))
				|| (p_y + p_bbox_top >= y + sprite_get_bbox_top(sprite_index) //in the middle
				&& p_y + p_bbox_bottom >= y + sprite_get_bbox_bottom(sprite_index) )){
			
			if (p_x + p_bbox_left <= x + sprite_get_bbox_left(sprite_index)){
				//player is to the left
				if (p_x + p_bbox_right >= x + sprite_get_bbox_left(sprite_index) - melee_rng){
					//attack if in melee range
					p.hp -= melee_dmg;
					cooldown = melee_cooldown;
				}
			} else if (p_x + p_bbox_left >= x + sprite_get_bbox_left(sprite_index)){
				//plaer is to the right
				if (p_x + p_bbox_left <= x + sprite_get_bbox_right(sprite_index) + melee_rng){
					//attack if in melee range
					p.hp -= melee_dmg;
					cooldown = melee_cooldown;
				}
			}
		}
	}
}


if  (ff) {
	v_y += a_g * dt;
}

collision_scr(dt);

