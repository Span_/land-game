{
    "id": "9d9746c3-f2aa-499e-87b6-b35feff29607",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemy_obj",
    "eventList": [
        {
            "id": "a2f80540-c56f-495f-9376-d734daa31973",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d9746c3-f2aa-499e-87b6-b35feff29607"
        },
        {
            "id": "729c4046-efd5-4fce-8688-72eb1b0b5159",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d9746c3-f2aa-499e-87b6-b35feff29607"
        },
        {
            "id": "c4d9ba75-b993-4593-85fc-c76960879edb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9d9746c3-f2aa-499e-87b6-b35feff29607"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "69836573-ca16-4ea9-a24a-cceef5dd426f",
    "visible": true
}