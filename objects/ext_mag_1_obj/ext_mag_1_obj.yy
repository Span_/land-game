{
    "id": "0b1b1765-74dc-4921-9804-88ce6d5808e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ext_mag_1_obj",
    "eventList": [
        {
            "id": "2d12da84-9bd0-463a-b6eb-546fc1c11657",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0b1b1765-74dc-4921-9804-88ce6d5808e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f4c2562-3c1c-4b26-83b7-ebc118aa8d7d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0fe69be9-e7f0-4f07-b6e9-015297e19f6f",
    "visible": true
}