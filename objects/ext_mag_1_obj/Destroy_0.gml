/// @description player benefit


var p = instance_nearest(x,y,player_obj);
if (p != noone) {
	with (p) {
		if (ext_mag_lvl == 0) {
			ext_mag_lvl = 1;
			ammo_max += global.ext_mag_1_value;
		}
		if (ammo + global.ext_mag_1_value > ammo_max) {
			ammo = ammo_max;
		} else ammo += global.ext_mag_1_value;
	}
}