/// @description player step

//handle exp gains
if (experience >= exp_req) {
	exp_level ++;
	exp_req = ceil(exp_req * exp_req_mod);
	hp_max += hp_per_level;
	hp = hp_max;
	experience = 0;
}

//handle input
var dt = delta_time / global.msec; //time since last tick in seconds

var a_cur = a_x_max;//current acceleration
var v_x_max_cur = v_x_max; //current max speed
if (ff) {
	a_cur = a_x_ff;//free fall acceleration x
	crouch = false;
	
} else if (keyboard_check(global.hk_crouch)) {
	a_cur = a_x_c;//crouching acceleration
	v_x_max_cur = v_x_c;
	crouch = true;
} else {
	crouch = false;
}

if(keyboard_check(global.hk_left)) {
	//if turning around
	if (v_x > 0.0 && !ff) {
		v_x = 0.0;
	}
	v_x = accelerate_scr(v_x,-1 * v_x_max_cur, a_cur,dt);
	//if( -1.0 * v_x < v_x_max_cur) {
	//	//do last bit of accelleration, careful not to go above max speed
	//	//var t = delta_time;
	//	if ( -1.0 * v_x + a_cur * dt > v_x_max_cur) v_x  = -1.0 * v_x_max_cur;
	//	else v_x -= a_cur * dt;
	//}else if (-1.0 * v_x > v_x_max_cur) {
	//	//if going too fast, slow down - important when crouching while running
	//	if ( -1.0 * v_x - a_cur * dt < v_x_max_cur) v_x  = -1.0 * v_x_max_cur;
	//	else v_x += a_cur * dt;
	//}
} else if(keyboard_check(global.hk_right)) {
	if (v_x < 0.0 && !ff) {
		v_x = 0.0;
	}
	v_x = accelerate_scr(v_x,v_x_max_cur,a_cur,dt);
	//if( v_x < v_x_max_cur) {
	//	if ( v_x + a_cur * dt > v_x_max_cur) v_x  = v_x_max_cur;
	//	else v_x += a_cur * dt;
	//}else if (v_x > v_x_max_cur) {
	//	//if going too fast, slow down - important when crouching while running
	//	if ( v_x - a_cur * dt < v_x_max_cur) v_x  = v_x_max_cur;
	//	else v_x -= a_cur * dt;
	//}
} else if (!ff){ //slow to a stop
	
	v_x = accelerate_scr(v_x,0.0,a_cur,dt);
	//if(v_x > 0.0) {
	//	if (v_x - a_cur * dt < 0.0) v_x = 0.0;
	//	else v_x -= a_cur * dt;
	//} else if(v_x < 0.0) {
	//	if (v_x + a_cur * dt > 0.0) v_x = 0.0;
	//	else v_x += a_cur * dt;
	//}
	
}


//jumping
if (keyboard_check_pressed(global.hk_jump)) {
	if(!ff) {
		ff = true; //start falling
		v_y = -1.0 * v_j;
	} else if (noone != collision_line(x + sprite_get_bbox_left(sprite_index) - 1,
						y + sprite_get_bbox_top(sprite_index),
						x + sprite_get_bbox_left(sprite_index) - 1,
						y + sprite_get_bbox_bottom(sprite_index),collidable_obj,true,true)){
		//do wall jump from left side
		v_y = -1.0 * v_j / sqrt(2.0);
		v_x = v_j / sqrt(2.0);
	} else if (noone != collision_line(x + sprite_get_bbox_right(sprite_index) + 1,
						y + sprite_get_bbox_top(sprite_index),
						x + sprite_get_bbox_right(sprite_index) + 1,
						y + sprite_get_bbox_bottom(sprite_index),collidable_obj,true,true)){
		//do wall jump from right side
		v_y = -1.0 * v_j / sqrt(2.0);
		v_x = -1.0 * v_j / sqrt(2.0);
	}
}
//else if (ff){
//	if(position_meeting(x,y,floor_obj)) {
//		//collision with foloore
//		v_y = 0;
//		ff = false;
//	} else {
		
//		//fall due to gravity
//		v_y += a_g * dt;
//	}
//} else if (!ff && !position_meeting(x,y,floor_obj)) {
//	//fall off the edge
//	ff = true;
//	v_y += a_g * dt;
	
//}

//fall due to gravity
if  (ff) {
	v_y += a_g * dt;
}

collision_scr(dt);


//attacking
if (cooldown > 0.0) {
	cooldown -= dt;
} else {
	
	if (keyboard_check(global.hk_shoot_left)){
		//spawn_loot_scr(loot_obj,x,y);
		if (ammo > 0){
			ammo --;
			attack_scr(x + sprite_get_bbox_left(sprite_index),
					y + sprite_get_bbox_top(sprite_index),
					0,y + sprite_get_bbox_top(sprite_index),
					shoot_cooldown,shoot_dmg);
		}
	} else if (keyboard_check(global.hk_shoot_right)){
		if (ammo > 0){
			ammo --;
			attack_scr(x + sprite_get_bbox_right(sprite_index),
					y + sprite_get_bbox_top(sprite_index),
					room_width - 1,y + sprite_get_bbox_top(sprite_index),
					shoot_cooldown,shoot_dmg);
		}
	} else if (keyboard_check(global.hk_melee_left)){
		attack_scr(x + sprite_get_bbox_left(sprite_index), y + sprite_get_bbox_top(sprite_index),
					x + sprite_get_bbox_left(sprite_index) - melee_rng, 
					y + sprite_get_bbox_top(sprite_index),
					melee_cooldown, melee_dmg);
	} else if (keyboard_check(global.hk_melee_right)){
		attack_scr(x + sprite_get_bbox_right(sprite_index), y + sprite_get_bbox_top(sprite_index),
					x + sprite_get_bbox_right(sprite_index) + melee_rng, 
					y + sprite_get_bbox_top(sprite_index),
					melee_cooldown, melee_dmg);
	} else if (keyboard_check_pressed(global.hk_reload)){
		if (ammo < ammo_max){
			cooldown = reload_cooldown;
			reloading = true;
		}
	}
}
if (cooldown <= 0.0) {
	
	cooldown = 0.0;
	if(reloading) {
		reloading = false;
		ammo = ammo_max;
	}
}