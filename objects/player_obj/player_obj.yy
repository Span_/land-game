{
    "id": "15f56a82-7ec5-4676-aee6-dabe8e315bbc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_obj",
    "eventList": [
        {
            "id": "8c7e5413-a7e8-4282-92b6-929a2d788e55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15f56a82-7ec5-4676-aee6-dabe8e315bbc"
        },
        {
            "id": "bcfd9a27-3aaf-49ea-8faa-65faf78edbb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15f56a82-7ec5-4676-aee6-dabe8e315bbc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "1910336b-cd99-4861-b71a-70bc215962d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8d9421fd-e42f-44cb-9879-829f5050f28c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "b59ba544-6d71-4b60-8e7a-d6165a5504d4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "3e30c21a-9ada-465d-a6c5-a8d30995e4b9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "604c3885-a1f5-4127-adbc-2af797813544",
    "visible": true
}