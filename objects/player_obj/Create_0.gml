/// @description 

//phy_linear_velocity_x = 0;

//m/s
v_x = 0.0;
v_y = 0.0;

//m/s^2
a_x = 0.0;
a_y = 0.0;

//m/s max speed
v_x_max = 6.0; //max speed while running
v_x_c = 1.0; //speed while crouching
//m/s^2
a_x_max = 100.0; //acceleration by running
a_x_ff = 10.0; //accelation while in free fall
a_x_c = 1.0; //acceleration while crouching

//gravity acceleration
a_g = 10.0;
//jump initial speed
v_j = 6.0;

ff = false; //free fall

crouch = false;

mass = 90.0; //kg

hp = 100;
hp_max = 100;
hp_per_level = 10;

experience = 0;
exp_level = 0;
exp_req = 100; //exp required to increase level
exp_req_mod = 1.25; //percent that exp_req increases per level

shoot_dmg = 25;
shoot_cooldown = 0.25; //seconds

ammo = 10;
ammo_max = 11;
reload_cooldown = 3.0;
reloading = false;

melee_dmg = 50;
melee_rng = 16;//px
melee_cooldown = 0.5; //seconds

cooldown = 0.0;

//player upgrades:
ext_mag_lvl = 0;
