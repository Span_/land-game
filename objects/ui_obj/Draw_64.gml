/// @description 


//draw stats
var txt = "";
var p = instance_nearest(0,0,player_obj);

if (p != noone) {
	with (p) {
		txt += string(hp) + " hp\n";
		txt += string(cooldown) + " sec\n";
		txt += string(ammo) + "/" + string(ammo_max) + " rounds\n";
		txt += string(delta_time/global.msec) + " sec\n"; //time since last tick
		txt += string(p.v_x) + " m/s\n";
		txt += string(experience) + " exp\n";
		txt += string(exp_req) + " exp req for level " + string(exp_level + 1) + "\n";
	}
}
draw_text(0,0,txt);