{
    "id": "d9d7802e-1549-47e2-a34e-3020c1403636",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "grass_obj",
    "eventList": [
        {
            "id": "dd88e383-4d1b-43df-b855-6120134671e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9d7802e-1549-47e2-a34e-3020c1403636"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "182a3300-60bd-436b-99d5-8aa9522853c0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2ab7267d-d23c-4725-ae3d-81cbee4516c6",
    "visible": true
}